'use strict'

const express = require('express');
const clientesController = require('../controllers/clientes');
const cuentasController = require('../controllers/cuentas');
const sesionesController = require('../controllers/sesiones');
const transferenciasController = require('../controllers/transferencias');
const parametrosController = require('../controllers/parametros');
const seg = require('../middlewares');
const api = express.Router();

const cors = require('cors');
api.use(cors()); //Alow all cors request

//clientes'
api.get('/clientes', clientesController.getAll);
api.post('/clientes', seg.isAuth, clientesController.save);
api.put('/clientes/:id', seg.isAuth, clientesController.update);
api.delete('/clientes/:id', seg.isAuth, clientesController.remove);
api.get('/clientes/:id', seg.isAuth, clientesController.getByID);
api.get('/clientes/numeroCuenta/:numeroCuenta', clientesController.getClienteByCuenta);
api.get('/clientes/:tipoDocumento/:numeroDocumento', seg.isAuth, clientesController.getClienteByDocumento);

//cuenta
api.get('/cuentas', cuentasController.getAll);
api.post('/cuentas', seg.isAuth, cuentasController.save);
api.put('/cuentas/:id', seg.isAuth, cuentasController.update);
api.delete('/cuentas/:id', seg.isAuth, cuentasController.remove);
api.get('/cuentas/:id', seg.isAuth, cuentasController.getByID);
api.get('/cuentas/numero/:numeroCuenta', seg.isAuth, cuentasController.getCuentaByNumero);
api.get('/cuentas/cliente/:idCliente', seg.isAuth, cuentasController.getCuentasByIdCliente);

//Sessiones
api.get('/sesiones', sesionesController.getAll);
api.post('/sesiones/logout', sesionesController.logout);
api.post('/sesiones/login', sesionesController.login);
api.post('/sesiones', seg.isAuth, sesionesController.save);
api.put('/sesiones/:id', seg.isAuth, sesionesController.update);
api.delete('/sesiones/:id', seg.isAuth, sesionesController.remove);
api.get('/sesiones/:id', seg.isAuth, sesionesController.getByID);
api.get('/sesiones/cliente/:idCliente', seg.isAuth, sesionesController.getSesionesByIdCliente);
//api.get('/pubnub', sesionesController.testPubNub);

//Transferencia
api.get('/transferencias', transferenciasController.getAll);
api.post('/transferencias/transferir', seg.isAuth, transferenciasController.transferir);

//Parametros
api.get('/parametros', parametrosController.getAll);

module.exports = api;
