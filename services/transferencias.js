var axios = require('axios');
const moment = require('moment');
const config = require('../config');
const cuentasServices = require('./cuentas');

//get All
async function getAll() {
    try {
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_TRANSFERENCIAS + '?' + config.MLAB_KEY;
        const response = await axios.get(uri);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

//Get by id
async function getByID(id) {
    try {
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_TRANSFERENCIAS + "/" + id + '?' + config.MLAB_KEY;
        const response = await axios.get(uri);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

//post save
async function save(data) {
    try {
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_TRANSFERENCIAS + '?' + config.MLAB_KEY;
        const response = await axios.post(uri, data);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

//put update
async function update(id, data) {
    try {
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_TRANSFERENCIAS + '/' + id + '?' + config.MLAB_KEY;
        var set = { "$set": data };
        const response = await axios.put(uri, set);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

//delete remove
async function remove(request, response) {
    try {
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_TRANSFERENCIAS + '/' + request.params.id + '?' + config.MLAB_KEY;
        const response = await axios.delete(uri);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

//get Movimientos by Id de cliente
async function realizarTransferencia(data) {
    try {
        let validations = [];

        //validar cuenta Origen
        let cuentaOrigen = await cuentasServices.getCuentaByNumero(data.cuentaOrigen);
        if (!cuentaOrigen) {
            validations.push({param: "cuentaOrigen", msg: "Cuenta Origen no existe" })
        } else {
            if (cuentaOrigen.saldoDisponible < data.importe) {
                validations.push({ "cuentaOrigen": "Saldo Insuficiente" })
            }
        }

        //validar cuenta Destino
        let cuentaDestino = await cuentasServices.getCuentaByNumero(data.cuentaDestino);
        if (!cuentaDestino) {
            validations.push({param:"cuentaDestino", msg: "Cuenta Destino no existe" })
        }

        //Validacion de moneda
        if (cuentaOrigen && cuentaDestino && cuentaOrigen.moneda !== cuentaDestino.moneda) {
            validations.push({ param:"cuentaDestino", msg: "Cuenta Destino en " + cuentaDestino.moneda });
        }

        if (validations.length > 0) {
            return validations;
        }

        let now = moment();
        let nowString = now.format('DD/MM/YYYY HH:MM');
        let impuestos = data.importe >= 1000 ? data.importe * config.IMPUESTO_ITF : 0.0;

        //procedemos con actualización de saldos y movimientos de cuenta Origen
        cuentaOrigen.saldoContable -= data.importe;
        cuentaOrigen.saldoDisponible -= data.importe;
        cuentaOrigen.movimientos.splice(0, 0, {
            fecha: nowString,
            glosa: config.GLOSA_TRANSFERENCIA,
            tipo: config.TIPO_MOV_DEBITO,
            moneda: cuentaOrigen.moneda,
            impuestos: impuestos,
            importe: data.importe,
            createdAt: now
        })
        await cuentasServices.update(cuentaOrigen._id.$oid, {
            saldoContable: cuentaOrigen.saldoContable,
            saldoDisponible: cuentaOrigen.saldoDisponible,
            movimientos: cuentaOrigen.movimientos
        });


        //procedemos con actualización de saldos y movimientos de cuenta Destino
        cuentaDestino.saldoContable -= data.importe;
        cuentaDestino.saldoDisponible -= data.importe;
        cuentaDestino.movimientos.splice(0, 0, {
            glosa: config.GLOSA_TRANSFERENCIA,
            fecha: nowString,
            tipo: config.TIPO_MOV_ABONO,
            moneda: cuentaDestino.moneda,
            impuestos: 0.0,
            importe: data.importe - impuestos,
            createdAt: now
        })
        await cuentasServices.update(cuentaDestino._id.$oid, {
            saldoContable: cuentaDestino.saldoContable,
            saldoDisponible: cuentaDestino.saldoDisponible,
            movimientos: cuentaDestino.movimientos
        });

        //registrar Transferencia
        await this.save({
            ...data,
            fecha: now,
            impuesto: impuestos,
            createdAt: now,
            glosa: data.glosa || config.GLOSA_TRANSFERENCIA
        })

        return validations;
    } catch (error) {
        console.log(error);
        throw error;
    }
}

module.exports = {
    getAll,
    getByID,
    save,
    update,
    remove,
    realizarTransferencia
}