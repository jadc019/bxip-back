'use strict'

const axios = require('axios');
const config = require('./../config');
const utils = require('./../utils');

async function getAll() {
    try {
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_SESIONES + '?' + config.MLAB_KEY;
        const response = await axios.get(uri);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

async function getByID(id) {
    try {
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_SESIONES + "/" + id + '?' + config.MLAB_KEY;
        const response = await axios.get(uri);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

//save
async function save(data) {
    try {
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_SESIONES + '?' + config.MLAB_KEY;
        const response = await axios.post(uri, data);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

//update
async function update(id, data) {
    try {
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_SESIONES + '/' + id + '?' + config.MLAB_KEY;
        let set = { $set: data };
        const response = await axios.put(uri, set);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

//remove
async function remove(id) {
    try {
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_SESIONES + '/' + id + '?' + config.MLAB_KEY;
        const response = await axios.delete(uri);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

//get sesion by token
async function getSesionByToken(token) {
    try {
        let query = 'q={"token":"' + token + '"}&';
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_SESIONES + '?' + query + config.MLAB_KEY;
        const response = await axios.get(uri);
        return response.data && response.data.length ? response.data[0] : null;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

//get sesiones by Id de cliente
async function getSesionesByIdCliente(idCliente) {
    try {
        const queryName = 'q={"idCliente":"' + idCliente + '", "fechaFin":null}&';
        let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_SESIONES + '?' + queryName + config.MLAB_KEY;
        const response = await axios.get(uri);
        return response.data;
    } catch (error) {
        return utils.hanldeErrorAxios(error);
    }
}

module.exports = {
    getAll,
    getByID,
    save,
    update,
    remove,
    getSesionByToken,
    getSesionesByIdCliente
}