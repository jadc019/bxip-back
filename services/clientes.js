'use strict'

const axios = require('axios');
const config = require('./../config');
const utils = require('./../utils');
const cuentasServices = require('./cuentas');

async function getAll() {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CLIENTES + '?' + config.MLAB_KEY;
    const response = await axios.get(uri);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

//Get by id
async function getByID(id) {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CLIENTES + "/" + id + '?' + config.MLAB_KEY;
    const response = await axios.get(uri);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

//post save
async function save(data) {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CLIENTES + '?' + config.MLAB_KEY;
    const response = await axios.post(uri, data);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

//put update
async function update(id, data) {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CLIENTES + '/' + id + '?' + config.MLAB_KEY;
    var set = { "$set": data };
    const response = await axios.put(uri, set);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
};

//delete remove
async function remove(id) {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CLIENTES + '/' + id + '?' + config.MLAB_KEY;
    const response = await axios.delete(uri);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

async function findByDocumento(tipoDocumento, numeroDocumento) {
  try {
    const queryName = 'q={"tipoDocumento":"' + tipoDocumento + '","numeroDocumento":"' + numeroDocumento + '"}&';
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CLIENTES + '?' + queryName + config.MLAB_KEY;
    const response = await axios.get(uri);
    return response.data && response.data.length ? response.data[0] : null;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

async function findClienteByCuenta(numeroCuenta) {
  try {
    let cuenta = await cuentasServices.getCuentaByNumero(numeroCuenta);
    if(!cuenta)
      return null;

    let cliente = await this.getByID(cuenta.idCliente);
    return cliente;
  } catch (error) {
    console.log(error);
    throw error;
  }
}

module.exports = {
  getAll,
  getByID,
  save,
  update,
  remove,
  findByDocumento,
  findClienteByCuenta
}