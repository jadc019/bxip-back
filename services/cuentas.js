'use strict'

const axios = require('axios');
const config = require('./../config');
const utils = require('./../utils');

//get All
async function getAll() {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CUENTAS + '?' + config.MLAB_KEY;
    const response = await axios.get(uri);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

//Get by id
async function getByID(id) {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CUENTAS + "/" + id + '?' + config.MLAB_KEY;
    const response = await axios.get(uri);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

//post save
async function save(data) {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CUENTAS + '?' + config.MLAB_KEY;
    const response = await axios.post(uri, data);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

//put update
async function update(id, data) {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CUENTAS + '/' + id + '?' + config.MLAB_KEY;
    var set = { "$set": data };
    const response = await axios.put(uri, set);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

//delete remove
async function remove(request, response) {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CUENTAS + '/' + request.params.id + '?' + config.MLAB_KEY;
    const response = await axios.delete(uri);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

//get Movimientos by #cuenta
async function getCuentaByNumero(numeroCuenta) {
  try {
    const query = 'q={"numeroCuenta":"' + numeroCuenta + '"}&';
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CUENTAS + '?' + query + config.MLAB_KEY;
    const response = await axios.get(uri);
    return response.data && response.data.length ? response.data[0] : null;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

//get Movimientos by Id de cliente
async function getCuentasByIdCliente(idCliente) {
  try {
    const query = 'q={"idCliente":"' + idCliente + '"}&f={"tipoCuenta:0,numeroCuenta":0}&';
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_CUENTAS + '?' + query + config.MLAB_KEY;
    const response = await axios.get(uri);
    return response.data || [];
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

module.exports = {
  getAll,
  getByID,
  save,
  update,
  remove,
  getCuentaByNumero,
  getCuentasByIdCliente
}