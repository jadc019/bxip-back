'use strict'

var axios = require('axios');
var config = require('./../config');

async function getAll() {
  try {
    let uri = config.MLAB_URL_BASE + config.MLAB_COLLECTION_PARAMETROS + '?' + config.MLAB_KEY;
    const response = await axios.get(uri);
    return response.data;
  } catch (error) {
    return utils.hanldeErrorAxios(error);
  }
}

module.exports = {
  getAll
}