'use strict'

const parametrosServices = require('../services/parametros');

//get All
function getAll(request, response) {
  parametrosServices.getAll().then(result => {
    var json = {};
    result.forEach(element => {
      json[element.nombre] = element.valor;
    });
    response.send(json);
  });
}

module.exports = {
  getAll
}