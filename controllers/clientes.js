'use strict'

const config = require('../config');
const clientesServices = require('../services/clientes');

//get All
function getAll(request, response) {
  clientesServices.getAll().then(result => {
    response.send(result);
  });
}

//Get by id
function getByID(request, response) {
  clientesServices.getByID(request.params.id).then(result => {
    if (!result)
      response.status(config.HTTP_CODE_404).send({ message: "Cliente no Existe" });
    else
      response.send(result);
  });
}

//post save
function save(request, response) {
  clientesServices.save(request.body).then(result => {
    response.status(config.HTTP_CODE_201).send(result);
  }).catch( error => {
    response.status(config.HTTP_CODE_500).send(error);
  });
}

//put update
function update(request, response) {
  clientesServices.update(request.params.id, request.body).then(result => {
    if(!result) 
      response.status(config.HTTP_CODE_404).send({ message: "Cliente no Existe" });
    else
      response.send(result);
  })
}

//delete remove
function remove(request, response) {
  clientesServices.remove(request.params.id).then(result => {
    if(!result) 
      response.status(config.HTTP_CODE_404).send({ message: "Cliente no Existe" });
    else
      response.send(result);
  })
}

//get Cliente by tipo y num de Documento
function getClienteByDocumento(request, response) {
  //Validaciones
  request.check("tipoDocumento", "tipo Documento inválido").isLength({ min: 3, max: 4 });
  request.check("numeroDocumento", "número Documento inválido").isLength({ size: 8 });
  request.check("numeroDocumento", "número Documento inválido").isInt();
  var errors = request.validationErrors();
  if (errors) {
    response.status(config.HTTP_CODE_400).send(errors);
    return;
  }

  let { tipoDocumento, numeroDocumento } = request.params;
  clientesServices.findByDocumento(tipoDocumento, numeroDocumento).then(result => {
    if (!result)
      response.status(config.HTTP_CODE_404).send({ message: "Cliente no Existe" });
    else
      response.send(result);
  });
}

function getClienteByCuenta(request, response){
  clientesServices.findClienteByCuenta(request.params.numeroCuenta).then(result => {
    if (!result)
      response.status(config.HTTP_CODE_404).send({ message: "Cuenta erronea" });
    else
      response.send(result);
  });
}

module.exports = {
  getAll,
  getByID,
  save,
  update,
  remove,
  getClienteByDocumento,
  getClienteByCuenta
};
