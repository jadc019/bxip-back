'use strict'

const config = require('../config');
const transferenciasService = require('./../services/transferencias');
const cuentasServices = require('./../services/cuentas');
const pubnub = require('./../utils/pubnub');

//get All
function getAll(request, response) {
    transferenciasService.getAll().then(result => {
        response.send(result);
    });
}

async function transferir(request, response) {
    //Validaciones
    request.check("cuentaOrigen", "Cuenta Origen incorrecta").isLength({ min: 16, max: 16 });
    request.check("cuentaDestino", "Cuenta Destino incorrecta").isLength({ min: 16, max: 16 });
    request.check("importe", "Importe debe ser mayor a 0").isDecimal();
    var errors = request.validationErrors();
    if (errors) {
        response.status(config.HTTP_CODE_400).send(errors);
        return;
    }

    if (typeof request.body.importe == "string")
        request.body.importe = parseFloat(request.body.importe);

    let validations = await transferenciasService.realizarTransferencia(request.body)
    if (validations.length) {
        response.status(config.HTTP_CODE_400).send(validations);
        return;
    }

    let cuentaOrigen = await cuentasServices.getCuentaByNumero(request.body.cuentaOrigen);
    let cuentaDestino = await cuentasServices.getCuentaByNumero(request.body.cuentaDestino);

    //Notificamos a Clientes
    pubnub.publishCientes({
        idCliente: cuentaOrigen.idCliente,
        evento: config.PUBNUB_EVENTO_TRANSFERENCIA
    })
    pubnub.publishCientes({
        idCliente: cuentaDestino.idCliente,
        evento: config.PUBNUB_EVENTO_TRANSFERENCIA
    })

    response.status(config.HTTP_CODE_201).send({ message: "OK" });
}

module.exports = {
    getAll,
    transferir
};
