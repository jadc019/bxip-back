'use strict'

const config = require('../config');
const cuentasServices = require('../services/cuentas');

//get All
function getAll(request, response) {
  cuentasServices.getAll().then(result => {
    response.send(result);
  });
}

//Get by id
function getByID(request, response) {
  cuentasServices.getByID(request.params.id).then(result => {
    if (!result)
      response.status(config.HTTP_CODE_404).send({ message: "Cuenta no Existe" });
    else
      response.send(result);
  });
}

//post save
function save(request, response) {
  cuentasServices.save(request.body).then(result => {
    response.status(config.HTTP_CODE_201).send(result);
  }).catch(error => {
    response.status(config.HTTP_CODE_500).send(error);
  });
}

//put update
function update(request, response) {
  cuentasServices.update(request.params.id, request.body).then(result => {
    if (!result)
      response.status(config.HTTP_CODE_404).send({ message: "Cuenta no Existe" });
    else
      response.send(result);
  })
}

//delete remove
function remove(request, response) {
  cuentasServices.remove(request.params.id).then(result => {
    if (!result)
      response.status(config.HTTP_CODE_404).send({ message: "Cuenta no Existe" });
    else
      response.send(result);
  })
}

//get cuentas por numero
function getCuentaByNumero(request, response) {
  cuentasServices.getCuentaByNumero(request.params.numeroCuenta).then(result => {
    if (!result)
      response.status(config.HTTP_CODE_404).send({ message: "Cuenta no Existe" });
    else
      response.send(result);
  });
}

//get cuentas por id de cliente
function getCuentasByIdCliente(request, response) {
  cuentasServices.getCuentasByIdCliente(request.params.idCliente).then(result => {
    response.send(result);
  });
}

module.exports = {
  getAll,
  getByID,
  save,
  update,
  remove,
  getCuentaByNumero,
  getCuentasByIdCliente
};
