'use strict'

const moment = require('moment');
const config = require('../config');
const sesionesService = require('./../services/sesiones');
const clientesService = require('./../services/clientes');
const utils = require('./../utils');
const pubnub = require('./../utils/pubnub');

//get All
function getAll(request, response) {
  sesionesService.getAll().then(result => {
    response.send(result);
  });
}

//Get by id
function getByID(request, response) {
  sesionesService.getByID(request.params.id).then(result => {
    if (!result)
      response.status(config.HTTP_CODE_404).send({ message: "Sesion no Existe" });
    else
      response.send(result);
  });
}

//post save
function save(request, response) {
  sesionesService.save(request.body).then(result => {
    response.status(config.HTTP_CODE_201).send(result);
  }).catch(error => {
    response.status(config.HTTP_CODE_500).send(error);
  });
}

//put update
function update(request, response) {
  sesionesService.update(request.params.id, request.body).then(result => {
    if (!result)
      response.status(config.HTTP_CODE_404).send({ message: "Sesion no Existe" });
    else
      response.send(result);
  })
}

//delete remove
function remove(request, response) {
  sesionesService.remove(request.params.id).then(result => {
    if (!result)
      response.status(config.HTTP_CODE_404).send({ message: "Sesion no Existe" });
    else
      response.send(result);
  })
}

//Login session
async function login(request, response) {
  //Validaciones
  request.check("tipoDocumento", "tipo Documento inválido").isLength({ min: 3, max: 4 });
  request.check("numeroDocumento", "número Documento inválido").isLength({ size: 8 });
  request.check("numeroDocumento", "número Documento inválido").isInt();
  request.check("password", "password es requerido").isLength({ min: 1 });
  var errors = request.validationErrors();
  if (errors) {
    response.status(config.HTTP_CODE_400).send(errors);
    return;
  }

  let { tipoDocumento, numeroDocumento, password, platform } = request.body;

  //buscamos cliente
  let cliente = await clientesService.findByDocumento(tipoDocumento, numeroDocumento);
  if (!cliente) {
    response.status(config.HTTP_CODE_400).send({message:"Usuario y/o Password incorrecto"});
    return;
  }

  //validamos password
  if (cliente.password !== password) {
    response.status(config.HTTP_CODE_400).send({message:"Usuario y/o Password incorrecto"});
    return;
  }

  //generamos token y registramos session
  let {fechaIni, fechaExp, token} = utils.createToken(cliente._id.$oid);
  let newSesion = {
    idCliente: cliente._id.$oid,
    nombre: cliente.nombres + ' ' + cliente.apellidos,
    documento: cliente.tipoDocumento + ' ' + cliente.numeroDocumento,
    fechaInicio: fechaIni,
    fechaFin: null,
    token: token,
    fechaExpiracion: fechaExp,
    host: request.headers.host,
    platform: platform,
    userAgent: request.headers['user-agent']
  }
  let result = await sesionesService.save(newSesion);

  //Notificamos a Cliente
  pubnub.publishCientes({
    idCliente: cliente._id.$oid,
    evento: config.PUBNUB_EVENTO_LOGIN
  })

  response.send(result);
}

//logout
async function logout(request, response) {
  //Decodificamos Token para obtener id de cliente
  let {idUser} = utils.getDataToken(request);
  if(!idUser)
    return;

  //obtenemos sesion de bd
  let sesionDB = await sesionesService.getSesionByToken(request.headers.authorization);
  if (!sesionDB || sesionDB.idCliente !== idUser)
    return;

  //actualizamos sesion
  let dataSet = { fechaFin: moment() };
  let result = await sesionesService.update(sesionDB._id.$oid, dataSet);

  //Notificamos a Cliente
  pubnub.publishCientes({
    idCliente: sesionDB.idCliente,
    evento: config.PUBNUB_EVENTO_LOGOUT
  })

  response.send(result);
}

//get sesiones por id cliente
function getSesionesByIdCliente(request, response) {
  sesionesService.getSesionesByIdCliente(request.params.idCliente).then(result => {
    response.send(result);
  })
}

function testPubNub(request, response){
  //Notificamos a Cliente
  pubnub.publishCientes({
    idCliente: '123123123123',
    evento: config.PUBNUB_EVENTO_LOGOUT
  }, result => {
    response.send(result);
  });
}

module.exports = {
  getAll,
  getByID,
  save,
  update,
  remove,
  login,
  logout,
  getSesionesByIdCliente,
  testPubNub
};
