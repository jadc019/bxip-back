'use strict'

const app = require('./app');
const config = require('./config');

app.listen(config.PORT, () => {
  console.log('API REST TECH U RUNNING ...');
});
