'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config');

function createToken(idUser) {
  let fechaIni = moment();
  let fechaExp = moment().add(1, 'hours');

  const payload = {
    idUser: idUser,
    iat: fechaIni.unix(),
    exp: fechaExp.unix(),
  };

  let token = jwt.encode(payload, config.SECRET_TOKEN);

  return { fechaIni: fechaIni, fechaExp: fechaExp, token: token };
}

function decodeToken(token) {
  return jwt.decode(token, config.SECRET_TOKEN);
}

function getDataToken(request) {
  if (!request.headers.authorization)
    return {};
  let token = request.headers.authorization;
  if (!token)
    return {};

  return this.decodeToken(token);
}

function valideToken(token) {
  const decode = new Promise((resolve, reject) => {
    try {
      const payload = jwt.decode(token, config.SECRET_TOKEN);
      if (payload.exp < moment().unix) {
        reject({
          status: 401,
          message: 'Token ha Expirado'
        })
      }
      resolve(payload.sub);
    } catch (err) {
      reject({
        status: 500,
        message: 'Invalid token'
      })
    }
  });
  return decode;
}

function hanldeErrorAxios(error) {
  if (error.response.status == config.HTTP_CODE_404)
    return null;
  else
    throw error.message;
}

module.exports = {
  createToken,
  decodeToken,
  getDataToken,
  valideToken,
  hanldeErrorAxios
};
