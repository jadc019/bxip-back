const PubNub = require('pubnub');
const config = require('./../config');

const pubnub = new PubNub({
    publishKey: config.PUBNUB_PUBLISH_KEY,
    subscribeKey: config.PUBNUB_SUSCRIBE_KEY,
    secretKey: config.PUBNUB_SECRET_KEY,
    ssl: true
})

function publishCientes(message, callback) {
    var publishConfig = {
        channel: config.PUBNUB_CHANNEL_CLIENTES,
        message: message
    }
    pubnub.publish(publishConfig, function (status, response) {
        console.log(status);
        if(callback) callback(response);
    })
}

module.exports = {
    publishCientes
}