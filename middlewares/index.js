'use strict'

const services = require('../utils');

function isAuth(req, res, next) {
  if (!req.headers.authorization)
    return res.status(403).send({ message: 'No tienes autorizacion' });

  let token = req.headers.authorization;
  if (!token)
    return res.status(403).send({ message: 'No tienes autorizacion' });

  services.valideToken(token)
    .then(response => {
      req.user = response;
      next();
    })
    .catch(response => {
      res.status(response.status).send(response.message);
    })
}

module.exports = {
  isAuth
};
